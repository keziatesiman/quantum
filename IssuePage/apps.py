from django.apps import AppConfig


class IssuepageConfig(AppConfig):
    name = 'IssuePage'
