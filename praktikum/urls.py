"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import dashboard.urls as dashboard
import aboutus.urls as aboutus
import IssuePage.urls as IssuePage
import totalIssue.urls as totalIssue
import aboutme.urls as aboutme
import commentIssue.urls as commentIssue
from django.views.generic import RedirectView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^dashboard/', include(dashboard, namespace='dashboard')),
    url(r'^aboutus/', include(aboutus, namespace='aboutus')),
    url(r'^IssuePage/', include(IssuePage, namespace='IssuePage')),
    url(r'^aboutme/', include(aboutme, namespace='aboutme')),
    url(r'^totalIssue/', include(totalIssue, namespace='totalIssue')),
    url(r'^commentIssue/', include(commentIssue, namespace='commentIssue')),

    url(r'^$', RedirectView.as_view(url= '/dashboard/', permanent='true'), name='redirect_landing_page')
]
